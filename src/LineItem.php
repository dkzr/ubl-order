<?php

namespace DKZR\UBL;

use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

use InvalidArgumentException;
use NumNum\UBL\Item;
use NumNum\UBL\Schema;
use NumNum\UBL\UnitCode;

class LineItem implements XmlSerializable
{
    public $xmlTagName = 'LineItem';
    protected $id;
    //protected $salesOrderID;
    //protected $uuID;
    protected $note;
    //protected $lineStatusCode;
    protected $quantity;
    protected $unitCode = UnitCode::UNIT;
    protected $unitCodeListId;
    //protected $lineExtensionAmount;
    //protected $totalTaxAmount;
    //protected $minimumQuantity;
    //protected $maximumQuantity;
    //protected $minimumBackorderQuantity;
    //protected $maximumBackorderQuantity;
    //protected $inspectionMethodCode;
    //protected $partialDeliveryIndicator;
    //protected $backOrderAllowedIndicator;
    //protected $accountingCostCode;
    //protected $accountingCost;
    //protected $warrantyInformation;
    //protected $delivery;
    //protected $deliveryTerms;
    //protected $originatorParty;
    //protected $orderedShipment;
    //protected $pricingReference;
    //protected $allowanceCharge;
    //protected $price;
    protected $item;
    //protected $subLineItem;
    //protected $warrantyValidityPeriod;
    //protected $warrantyParty;
    //protected $taxTotal;
    //protected $itemPriceExtension;
    //protected $lineReference;

    /**
     * @return mixed
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Order
     */
    public function setId(?string $id): LineItem
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @return float
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    /**
     * @param ?float $quantity
     * @return LineItem
     */
    public function setQuantity(?float $quantity): LineItem
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnitCode(): ?string
    {
        return $this->unitCode;
    }

    /**
     * @param string $unitCode
     * @return LineItem
     */
    public function setUnitCode(?string $unitCode): LineItem
    {
        $this->unitCode = $unitCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnitCodeListId(): ?string
    {
        return $this->unitCodeListId;
    }

    /**
     * @param string $unitCodeListId
     * @return LineItem
     */
    public function setUnitCodeListId(?string $unitCodeListId): LineItem
    {
        $this->unitCodeListId = $unitCodeListId;
        return $this;
    }

    /**
     * @return float
     */
    //public function getLineExtensionAmount(): ?float
    //{
    //    return $this->lineExtensionAmount;
    //}

    /**
     * @param float $lineExtensionAmount
     * @return LineItem
     */
    //public function setLineExtensionAmount(?float $lineExtensionAmount): LineItem
    //{
    //    $this->lineExtensionAmount = $lineExtensionAmount;
    //    return $this;
    //}

    /**
     * @param string $note
     * @return OrderLine
     */
    public function setNote(?string $note): OrderLine
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return Item
     */
    public function getItem(): ?Item
    {
        return $this->item;
    }

    /**
     * @param Item $item
     * @return InvoiceLine
     */
    public function setItem(Item $item): LineItem
    {
        $this->item = $item;
        return $this;
    }

    /**
     * The validate function that is called during xml writing to valid the data of the object.
     *
     * @return void
     * @throws InvalidArgumentException An error with information about required data that is missing to write the XML
     */
    public function validate()
    {
        if ($this->id === null) {
            throw new InvalidArgumentException('Missing lineItem id');
        }

        if ($this->item === null) {
            throw new InvalidArgumentException('Missing lineItem item');
        }
    }

    /**
     * The xmlSerialize method is called during xml writing.
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer): void
    {
        $this->validate();

        $writer->write([
            Schema::CBC . 'ID' => $this->id
        ]);

        if ($this->note !== null) {
            $writer->write([
                Schema::CBC . 'Note' => $this->note
            ]);
        }

        if ($this->quantity !== null) {
            $quantityAttributes = [
                'unitCode' => $this->unitCode,
            ];

            if ($this->unitCodeListId !== null) {
                $quantityAttributes['unitCodeListID'] = $this->unitCodeListId;
            }

            $writer->write([
                [
                    'name' => Schema::CBC . 'Quantity',
                    'value' => number_format($this->quantity, 2, '.', ''),
                    'attributes' => $quantityAttributes
                ]
            ]);
        }

        $writer->write([
            Schema::CAC . 'Item' => $this->item,
        ]);
    }
}
