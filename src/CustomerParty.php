<?php

namespace DKZR\UBL;

use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

use NumNum\UBL\Party;
use NumNum\UBL\Schema;

class CustomerParty implements XmlSerializable
{
    public $xmlTagName = 'CustomerParty';

    protected $customerAssignedAccountId;
    protected $customerAssignedAccountIdAttributes = [];
    protected $supplierAssignedAccountId;
    protected $supplierAssignedAccountIdAttributes = [];
    //protected $additionalAccountID;
    protected $party;
    //protected $deliveryContact;
    //protected $accountingContact;
    //protected $buyerContact;

    public function __construct(string $xmlTagName = 'CustomerParty')
    {
        $this->xmlTagName = $xmlTagName;
    }

    /**
     * @return mixed
     */
    public function getCustomerAssignedAccountId(): ?string
    {
        return $this->customerAssignedAccountId;
    }

    /**
     * @param mixed $customerAssignedAccountId
     * @return CustomerParty
     */
    public function setCustomerAssignedAccountId(?string $customerAssignedAccountId, ?array $attributes = null): CustomerParty
    {
        $this->customerAssignedAccountId = $customerAssignedAccountId;
        if (isset($attributes)) {
            $this->customerAssignedAccountIdAttributes = array_filter($attributes);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSupplierAssignedAccountId(): ?string
    {
        return $this->supplierAssignedAccountId;
    }

    /**
     * @param mixed $supplierAssignedAccountId
     * @return CustomerParty
     */
    public function setSupplierAssignedAccountId(?string $supplierAssignedAccountId, ?array $attributes = null): CustomerParty
    {
        $this->supplierAssignedAccountId = $supplierAssignedAccountId;
        if (isset($attributes)) {
            $this->supplierAssignedAccountIdAttributes = array_filter($attributes);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParty(): ?Party
    {
        return $this->party;
    }

    /**
     * @param mixed $party
     * @return CustomerParty
     */
    public function setParty(Party $party): CustomerParty
    {
        $this->party = $party;
        return $this;
    }

    /**
     * The xmlSerialize method is called during xml writing.
     *
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer): void
    {
        if ($this->customerAssignedAccountId != null) {
            $writer->write([
                [
                    'name' => Schema::CBC . 'CustomerAssignedAccountID',
                    'value' => $this->customerAssignedAccountId,
                    'attributes' => $this->customerAssignedAccountIdAttributes,
                ]
            ]);
        }

        if ($this->supplierAssignedAccountId != null) {
            $writer->write([
                [
                    'name' => Schema::CBC . 'SupplierAssignedAccountID',
                    'value' => $this->supplierAssignedAccountId,
                    'attributes' => $this->supplierAssignedAccountIdAttributes,
                ]
            ]);
        }

        if ($this->party != null) {
            $writer->write([
                Schema::CAC . 'Party' => $this->party
            ]);
        }
    }
}
