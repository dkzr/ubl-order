<?php

namespace DKZR\UBL;

use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;
use DateTime;

use NumNum\UBL\Schema;

class Period implements XmlSerializable
{
    public $xmlTagName = 'PeriodType';
    protected $startDate;
    //protected $startTime;
    //protected $endDate;
    //protected $endTime;
    //protected $durationMeasure;
    //protected $descriptionCode;
    //protected $description;

    /**
     * @return DateTime
     */
    public function getStartDate(): ?DateTime
    {
        return $this->startDate;
    }

    /**
     * @param DateTime $issueDate
     * @return PeriodType
     */
    public function setStartDate(DateTime $startDate): Period
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * The xmlSerialize method is called during xml writing.
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer): void
    {
        if ($this->startDate !== null) {
            $writer->write([
                Schema::CBC . 'StartDate' => $this->startDate->format('Y-m-d')
            ]);
        }
    }
}
