<?php

namespace DKZR\UBL;

use Sabre\Xml\Writer;

use NumNum\UBL\Schema;
use NumNum\UBL\Party as InvoiceParty;

class Party extends InvoiceParty
{
    private $partyIdentificationId;
    private $partyIdentificationIdAttributes = [];

    /**
     * @return string
     */
    public function getPartyIdentificationId(): ?string
    {
        return $this->partyIdentificationId;
    }

    /**
     * @param string $partyIdentificationId
     * @return Party
     */
    public function setPartyIdentificationId(?string $partyIdentificationId, ?array $attributes = null): Party
    {
        $this->partyIdentificationId = $partyIdentificationId;
        if (isset($attributes)) {
            $this->partyIdentificationIdAttributes = array_filter($attributes);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPartyIdentificationSchemeId(): ?string
    {
        return $this->partyIdentificationIdAttributes['schemeId'] ?? null;
    }

    /**
     * @param string $partyIdentificationSchemeId
     * @return Party
     */
    public function setPartyIdentificationSchemeId(?string $partyIdentificationSchemeId): Party
    {
        $this->partyIdentificationIdAttributes['schemeId'] = $partyIdentificationSchemeId;
        return $this;
    }

    /**
     * The xmlSerialize method is called during xml writing.
     *
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer): void
    {
        if ($this->partyIdentificationId !== null) {
            $writer->write([
                Schema::CAC . 'PartyIdentification' => [
                    [
                        'name' => Schema::CBC . 'ID',
                        'value' => $this->partyIdentificationId,
                        'attributes' => $this->partyIdentificationIdAttributes,
                    ]
                ],
            ]);
        }

        parent::xmlSerialize($writer);
    }
}
