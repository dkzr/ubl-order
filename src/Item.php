<?php

namespace DKZR\UBL;

use Sabre\Xml\Writer;

use NumNum\UBL\Schema;
use NumNum\UBL\Item as InvoiceItem;

class Item extends InvoiceItem
{
    private $sellersItemIdentification;
    private $sellersItemIdentificationAttributes = [];

    /**
     * @return mixed
     */
    public function getSellersItemIdentification(): ?string
    {
        return $this->sellersItemIdentification;
    }

    /**
     * @param mixed $sellersItemIdentification
     * @return Item
     */
    public function setSellersItemIdentification(?string $sellersItemIdentification, ?array $attributes = null): Item
    {
        $this->sellersItemIdentification = $sellersItemIdentification;
        if (isset($attributes)) {
            $this->sellersItemIdentificationAttributes = array_filter($attributes);
        }
        return $this;
    }
    /**
     * The xmlSerialize method is called during xml writing.
     *
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer): void
    {
        // hackisch, but needed because of how NumNum\UBL\Item::xmlSerialize is written
        $sellersItemIdentification = $this->sellersItemIdentification;
        $this->sellersItemIdentification = null;

        parent::xmlSerialize($writer);

        $this->sellersItemIdentification = $sellersItemIdentification;

        if (!empty($this->getSellersItemIdentification())) {
            $writer->write([
                Schema::CAC . 'SellersItemIdentification' => [
                    [
                        'name' => Schema::CBC . 'ID',
                        'value' => $this->sellersItemIdentification,
                        'attributes' => $this->sellersItemIdentificationAttributes,
                    ],
                ],
            ]);
        }

        if (!empty($this->getClassifiedTaxCategory())) {
            $writer->write([
                Schema::CAC . 'ClassifiedTaxCategory' => $this->getClassifiedTaxCategory()
            ]);
        }
    }
}
