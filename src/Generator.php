<?php

namespace DKZR\UBL;

use Sabre\Xml\Service;

use NumNum\UBL\Generator as InvoiceGenerator;

class Generator extends InvoiceGenerator
{
    public static function order(Order $order, $currencyId = 'EUR')
    {
        self::$currencyID = $currencyId;

        $xmlService = new Service();

        $xmlService->namespaceMap = [
            'urn:oasis:names:specification:ubl:schema:xsd:' . $order->xmlTagName . '-2' => '',
            'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2' => 'cbc',
            'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2' => 'cac'
        ];

        return $xmlService->write($order->xmlTagName, [
            $order
        ]);
    }
}
