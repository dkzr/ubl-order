<?php

namespace DKZR\UBL;

/**
 * All possible Unit Codes that can be used
 * To extend, see also: https://service.unece.org/trade/untdid/d95b/uncl/uncl1001.htm
 */
class OrderTypeCode
{
    public const ORDER = 220;
    public const BLANKET_ORDER = 221;
    public const CALL_OFF_ORDER = 226;
    public const CONSIGNMENT_ORDER = 227;
}
