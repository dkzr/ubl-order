<?php

namespace DKZR\UBL;

use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

use NumNum\UBL\Party;
use NumNum\UBL\Schema;

class SupplierParty implements XmlSerializable
{
    public $xmlTagName = 'SupplierParty';

    protected $customerAssignedAccountId;
    protected $customerAssignedAccountIdAttributes = [];
    //protected $additionalAccountID;
    protected $party;
    //protected $despatchContact;
    //protected $accountingContact;
    //protected $sellerContact;

    public function __construct(string $xmlTagName = 'SupplierParty')
    {
        $this->xmlTagName = $xmlTagName;
    }

    /**
     * @return mixed
     */
    public function getCustomerAssignedAccountId(): ?string
    {
        return $this->customerAssignedAccountId;
    }

    /**
     * @param mixed $customerAssignedAccountId
     * @return SupplierParty
     */
    public function setCustomerAssignedAccountId(?string $customerAssignedAccountId): SupplierParty
    {
        $this->customerAssignedAccountId = $customerAssignedAccountId;
        if (isset($attributes)) {
            $this->customerAssignedAccountIdAttributes = array_filter($attributes);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParty(): ?Party
    {
        return $this->party;
    }

    /**
     * @param mixed $party
     * @return SupplierParty
     */
    public function setParty(Party $party): SupplierParty
    {
        $this->party = $party;
        return $this;
    }

    /**
     * The xmlSerialize method is called during xml writing.
     *
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer): void
    {
        if ($this->customerAssignedAccountId != null) {
            $writer->write([
                [
                    'name' => Schema::CBC . 'CustomerAssignedAccountID',
                    'value' => $this->customerAssignedAccountId,
                    'attributes' => $this->customerAssignedAccountIdAttributes,
                ]
            ]);
        }

        if ($this->party != null) {
            $writer->write([
                Schema::CAC . 'Party' => $this->party
            ]);
        }
    }
}
