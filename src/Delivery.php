<?php

namespace DKZR\UBL;

use Sabre\Xml\Writer;

use NumNum\UBL\Delivery as InvoiceDelivery;
use NumNum\UBL\Schema;

class Delivery extends InvoiceDelivery
{
    protected $requestedDeliveryPeriod;

    public function setRequestedDeliveryPeriod(Period $requestedDeliveryPeriod): Delivery
    {
        $this->requestedDeliveryPeriod = $requestedDeliveryPeriod;
        return $this;
    }

    public function getRequestedDeliveryPeriod(): ?Period
    {
        return $this->requestedDeliveryPeriod;
    }

    /**
     * The xmlSerialize method is called during xml writing.
     *
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer): void
    {
        parent::xmlSerialize( $writer );

        if ($this->requestedDeliveryPeriod != null) {
            $writer->write([
               Schema::CAC . 'RequestedDeliveryPeriod' => $this->requestedDeliveryPeriod
            ]);
        }
    }
}
