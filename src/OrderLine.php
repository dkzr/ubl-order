<?php

namespace DKZR\UBL;

use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

use InvalidArgumentException;
use NumNum\UBL\Schema;

class OrderLine implements XmlSerializable
{
    public $xmlTagName = 'OrderLine';
    //protected $substitutionStatusCode;
    protected $note;
    protected $lineItem;
    //protected $sellerProposedSubstituteLineItem;
    //protected $sellerSubstitutedLineItem;
    //protected $buyerProposedSubstituteLineItem;
    //protected $catalogueLineReference;
    //protected $quotationLineReference;
    //protected $orderLineReference;
    //protected $documentReference;

    /**
     * @return string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return OrderLine
     */
    public function setNote(string $note): OrderLine
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return LineItem
     */
    public function getLineItem(): ?LineItem
    {
        return $this->lineItem;
    }

    /**
     * @param LineItem $lineItem
     * @return OrderLine
     */
    public function setLineItem(LineItem $lineItem): OrderLine
    {
        $this->lineItem = $lineItem;
        return $this;
    }

    /**
     * The validate function that is called during xml writing to valid the data of the object.
     *
     * @return void
     * @throws InvalidArgumentException An error with information about required data that is missing to write the XML
     */
    public function validate()
    {
        if ($this->lineItem === null) {
            throw new InvalidArgumentException('Missing orderLine lineItem');
        }
    }

    /**
     * The xmlSerialize method is called during xml writing.
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer): void
    {
        $this->validate();

        if ($this->note !== null) {
            $writer->write([
                Schema::CBC . 'Note' => $this->note
            ]);
        }

        $writer->write([
            Schema::CAC . 'LineItem' => $this->lineItem,
        ]);
    }
}
