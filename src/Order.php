<?php

namespace DKZR\UBL;

use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

use DateTime;
use InvalidArgumentException;
use NumNum\UBL\AdditionalDocumentReference;
use NumNum\UBL\AllowanceCharge;
use NumNum\UBL\Delivery;
use NumNum\UBL\PaymentMeans;
use NumNum\UBL\Schema;
use NumNum\UBL\TaxTotal;

class Order implements XmlSerializable
{
    public $xmlTagName = 'Order';
    protected $UBLVersionID = '2.1';
    protected $customizationID = '1.0';
    protected $profileID;
    //protected $profileExecutionID;
    protected $id;
    //protected $salesOrderID;
    protected $copyIndicator;
    //protected $uuID;
    protected $issueDate;
    //protected $issueTime;
    protected $orderTypeCode = OrderTypeCode::ORDER;
    protected $note;
    //protected $requestedInvoiceCurrencyCode = 'EUR';
    protected $documentCurrencyCode = 'EUR';
    //protected $pricingCurrencyCode = 'EUR';
    //protected $taxCurrencyCode = 'EUR';
    protected $customerReference;
    protected $accountingCostCode;
    //protected $accountingCost;
    //protected $lineCountNumeric;
    //protected $validityPeriod;
    //protected $quotationDocumentReference;
    //protected $orderDocumentReference;
    //protected $originatorDocumentReference;
    //protected $catalogueReference;
    protected $additionalDocumentReferences = [];
    //protected $contract;
    //protected $projectReference;
    //protected $signature;
    protected $buyerCustomerParty;
    protected $sellerSupplierParty;
    //protected $originatorCustomerParty;
    //protected $freightForwarderParty;
    //protected $accountingCustomerParty;
    protected $delivery;
    //protected $deliveryTerms;
    protected $paymentMeans;
    //protected $paymentTerms;
    //protected $transactionConditions;
    protected $allowanceCharges;
    //protected $taxExchangeRate;
    //protected $pricingExchangeRate;
    //protected $paymentExchangeRate;
    //protected $destinationCountry;
    protected $taxTotal;
    //protected $anticipatedMonetaryTotal;
    /** @var OrderLine[] $orderLines */
    protected $orderLines;

    /**
     * @return string
     */
    public function getUBLVersionID(): ?string
    {
        return $this->UBLVersionID;
    }

    /**
     * @param string $UBLVersionID
     * eg. '2.0', '2.1', '2.2', ...
     * @return Order
     */
    public function setUBLVersionID(?string $UBLVersionID): Order
    {
        $this->UBLVersionID = $UBLVersionID;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Order
     */
    public function setId(?string $id): Order
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param mixed $customizationID
     * @return Order
     */
    public function setCustomizationID(?string $customizationID): Order
    {
        $this->customizationID = $customizationID;
        return $this;
    }

    /**
     * @param mixed $profileID
     * @return Order
     */
    public function setProfileID(?string $profileID): Order
    {
        $this->profileID = $profileID;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCopyIndicator(): bool
    {
        return $this->copyIndicator;
    }

    /**
     * @param bool $copyIndicator
     * @return Order
     */
    public function setCopyIndicator(bool $copyIndicator): Order
    {
        $this->copyIndicator = $copyIndicator;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getIssueDate(): ?DateTime
    {
        return $this->issueDate;
    }

    /**
     * @param DateTime $issueDate
     * @return Order
     */
    public function setIssueDate(DateTime $issueDate): Order
    {
        $this->issueDate = $issueDate;
        return $this;
    }

    /**
     * @param mixed $currencyCode
     * @return Order
     */
    public function setDocumentCurrencyCode(string $currencyCode = 'EUR'): Order
    {
        $this->documentCurrencyCode = $currencyCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderTypeCode(): ?string
    {
        return $this->orderTypeCode;
    }

    /**
     * @param string $orderTypeCode
     * See also: src/OrderTypeCode.php
     * @return Order
     */
    public function setOrderTypeCode(string $orderTypeCode): Order
    {
        $this->orderTypeCode = $orderTypeCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return Order
     */
    public function setNote(string $note): Order
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return DeliveryTerms
     */
    //public function getDeliveryTerms(): ?DeliveryTerms
    //{
    //    return $this->deliveryTerms;
    //}

    /**
     * @param DeliveryTerms $deliveryTerms
     * @return Order
     */
    //public function setDeliveryTerms(DeliveryTerms $deliveryTerms): Order
    //{
    //    $this->deliveryTerms = $deliveryTerms;
    //    return $this;
    //}

    /**
     * @return SupplierParty
     */
    public function getSellerSupplierParty(): ?SupplierParty
    {
        return $this->sellerSupplierParty;
    }

    /**
     * @param SupplierParty $sellerSupplierParty
     * @return Order
     */
    public function setSellerSupplierParty(SupplierParty $sellerSupplierParty): Order
    {
        $this->sellerSupplierParty = $sellerSupplierParty;
        return $this;
    }

    /**
     * @return CustomerParty
     */
    public function getBuyerCustomerParty(): ?CustomerParty
    {
        return $this->buyerCustomerParty;
    }

    /**
     * @param CustomerParty $buyerCustomerParty
     * @return Order
     */
    public function setBuyerCustomerParty(CustomerParty $buyerCustomerParty): Order
    {
        $this->buyerCustomerParty = $buyerCustomerParty;
        return $this;
    }

    /**
     * @return PaymentMeans
     */
    public function getPaymentMeans(): ?PaymentMeans
    {
        return $this->paymentMeans;
    }

    /**
     * @param PaymentMeans $paymentMeans
     * @return Order
     */
    public function setPaymentMeans(PaymentMeans $paymentMeans): Order
    {
        $this->paymentMeans = $paymentMeans;
        return $this;
    }

    /**
     * @return TaxTotal
     */
    public function getTaxTotal(): ?TaxTotal
    {
        return $this->taxTotal;
    }

    /**
     * @param TaxTotal $taxTotal
     * @return Order
     */
    public function setTaxTotal(TaxTotal $taxTotal): Order
    {
        $this->taxTotal = $taxTotal;
        return $this;
    }

    /**
     * @return OrderLine[]
     */
    public function getOrderLines(): ?array
    {
        return $this->orderLines;
    }

    /**
     * @param OrderLine[] $orderLines
     * @return Order
     */
    public function setOrderLines(array $orderLines): Order
    {
        $this->orderLines = $orderLines;
        return $this;
    }

    /**
     * @return AllowanceCharge[]
     */
    public function getAllowanceCharges(): ?array
    {
        return $this->allowanceCharges;
    }

    /**
     * @param AllowanceCharge[] $allowanceCharges
     * @return Order
     */
    public function setAllowanceCharges(array $allowanceCharges): Order
    {
        $this->allowanceCharges = $allowanceCharges;
        return $this;
    }

    /**
     * @return AdditionalDocumentReference
     */
    public function getAdditionalDocumentReference(): ?AdditionalDocumentReference
    {
        return $this->additionalDocumentReferences[0] ?? null;
    }

    /**
     * @param AdditionalDocumentReference $additionalDocumentReference
     * @return Order
     */
    public function setAdditionalDocumentReference(AdditionalDocumentReference $additionalDocumentReference): Order
    {
        $this->additionalDocumentReferences = [$additionalDocumentReference];
        return $this;
    }

    /**
     * @param AdditionalDocumentReference $additionalDocumentReference
     * @return Order
     */
    public function addAdditionalDocumentReference(AdditionalDocumentReference $additionalDocumentReference): Order
    {
        $this->additionalDocumentReferences[] = $additionalDocumentReference;
        return $this;
    }

    /**
     * @param string $customerReference
     * @return Order
     */
    public function setCustomerReference(string $customerReference): Order
    {
        $this->customerReference = $customerReference;
        return $this;
    }

      /**
     * @return string customerReference
     */
    public function getCustomerReference(): ?string
    {
        return $this->customerReference;
    }

    /**
     * @return mixed
     */
    public function getAccountingCostCode(): ?string
    {
        return $this->accountingCostCode;
    }

    /**
     * @param mixed $accountingCostCode
     * @return Order
     */
    public function setAccountingCostCode(string $accountingCostCode): Order
    {
        $this->accountingCostCode = $accountingCostCode;
        return $this;
    }

    /**
     * @return Delivery
     */
    public function getDelivery(): ?Delivery
    {
        return $this->delivery;
    }

    /**
     * @param Delivery $delivery
     * @return Order
     */
    public function setDelivery(Delivery $delivery): Order
    {
        $this->delivery = $delivery;
        return $this;
    }

    /**
     * The validate function that is called during xml writing to valid the data of the object.
     *
     * @return void
     * @throws InvalidArgumentException An error with information about required data that is missing to write the XML
     */
    public function validate()
    {
        if ($this->id === null) {
            throw new InvalidArgumentException('Missing order id');
        }

        if (!$this->issueDate instanceof DateTime) {
            throw new InvalidArgumentException('Invalid order issueDate');
        }

        if ($this->orderTypeCode === null) {
            throw new InvalidArgumentException('Missing order orderTypeCode');
        }

        if ($this->buyerCustomerParty === null) {
            throw new InvalidArgumentException('Missing order buyerCustomerParty');
        }

        if ($this->sellerSupplierParty === null) {
            throw new InvalidArgumentException('Missing order sellerSupplierParty');
        }

        if ($this->orderLines === null) {
            throw new InvalidArgumentException('Missing order lines');
        }
    }

    /**
     * The xmlSerialize method is called during xml writing.
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer): void
    {
        $this->validate();

        $writer->write([
            Schema::CBC . 'UBLVersionID' => $this->UBLVersionID,
            Schema::CBC . 'CustomizationID' => $this->customizationID,
        ]);

        if ($this->profileID !== null) {
            $writer->write([
                Schema::CBC . 'ProfileID' => $this->profileID
            ]);
        }

        $writer->write([
            Schema::CBC . 'ID' => $this->id
        ]);

        if ($this->copyIndicator !== null) {
            $writer->write([
                Schema::CBC . 'CopyIndicator' => $this->copyIndicator ? 'true' : 'false'
            ]);
        }

        $writer->write([
            Schema::CBC . 'IssueDate' => $this->issueDate->format('Y-m-d'),
        ]);

        if ($this->orderTypeCode !== null) {
            $writer->write([
                Schema::CBC . $this->xmlTagName . 'TypeCode' => $this->orderTypeCode
            ]);
        }

        if ($this->note !== null) {
            $writer->write([
                Schema::CBC . 'Note' => $this->note
            ]);
        }

        $writer->write([
            Schema::CBC . 'DocumentCurrencyCode' => $this->documentCurrencyCode,
        ]);

        if ($this->accountingCostCode !== null) {
            $writer->write([
                Schema::CBC . 'AccountingCostCode' => $this->accountingCostCode
            ]);
        }

        if ($this->customerReference != null) {
            $writer->write([
                Schema::CBC . 'CustomerReference' => $this->customerReference
            ]);
        }

        if (!empty($this->additionalDocumentReferences)) {
            foreach ($this->additionalDocumentReferences as $additionalDocumentReference) {
                $writer->write([
                    Schema::CAC . 'AdditionalDocumentReference' => $additionalDocumentReference
                ]);
            }
        }

        $writer->write([
            Schema::CAC . 'SellerSupplierParty' => $this->sellerSupplierParty,
            Schema::CAC . 'BuyerCustomerParty' => $this->buyerCustomerParty,
        ]);

        if ($this->delivery != null) {
            $writer->write([
                Schema::CAC . 'Delivery' => $this->delivery
            ]);
        }

        if ($this->paymentMeans !== null) {
            $writer->write([
                Schema::CAC . 'PaymentMeans' => $this->paymentMeans
            ]);
        }

        //if ($this->deliveryTerms !== null) {
        //    $writer->write([
        //        Schema::CAC . 'DeliveryTerms' => $this->deliveryTerms
        //    ]);
        //}

        if ($this->allowanceCharges !== null) {
            foreach ($this->allowanceCharges as $allowanceCharge) {
                $writer->write([
                    Schema::CAC . 'AllowanceCharge' => $allowanceCharge
                ]);
            }
        }

        if ($this->taxTotal !== null) {
            $writer->write([
                Schema::CAC . 'TaxTotal' => $this->taxTotal
            ]);
        }

        foreach ($this->orderLines as $orderLine) {
            $writer->write([
                Schema::CAC . $orderLine->xmlTagName => $orderLine
            ]);
        }
    }
}
