<?php

namespace DKZR\UBL;

use Sabre\Xml\Reader as SabreReader;

class Reader extends SabreReader
{
    public function parse(): array
    {
        $this->namespaceMap = array_merge( [
            'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2' => 'cbc',
            'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2' => 'cac'
        ], $this->namespaceMap );

        return parent::parse();
    }

    /**
     * Returns the function that should be used to parse the element identified
     * by its clark-notation name.
     */
    public function getDeserializerForElementName(string $name): callable
    {
        if (
            ! array_key_exists($name, $this->elementMap)
            && (
                $this->depth === 0
                || str_starts_with( $name, '{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}' )
            )
        ) {
            return [ $this, 'deserializerUblKeyValue' ];
        } else {
            return parent::getDeserializerForElementName( $name );
        }
    }

    public function deserializerUblKeyValue():array
    {
        // If there's no children, we don't do anything.
        if ($this->isEmptyElement) {
            $this->next();

            return [];
        }

        if (!$this->read()) {
            $this->next();

            return [];
        }

        if (Reader::END_ELEMENT === $this->nodeType) {
            $this->next();

            return [];
        }

        $values = [];

        do {
            if (Reader::ELEMENT === $this->nodeType) {
                if ( array_key_exists( $this->namespaceURI, $this->namespaceMap ) ) {
                    $key   = $this->localName;
                } else {
                    $key   = $this->getClark();
                }
                $value = $this->parseCurrentElement()['value'];

                if ( array_key_exists( $key, $values ) ) {
                    // `array_is_list` is only supported by PHPv81+ so for now we just test the first key to be an integer
                    if ( is_array( $values[$key] ) && is_integer( key( $values[$key] ) ) ) {
                        $values[$key][] = $value;
                    } else {
                        $values[$key] = [
                            $values[$key],
                            $value,
                        ];
                    }
                } else {
                    $values[$key] = $value;
                }
            } else {
                if (!$this->read()) {
                    break;
                }
            }
        } while (Reader::END_ELEMENT !== $this->nodeType);

        $this->read();

        return $values;
    }
}
